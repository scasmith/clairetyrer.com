<?php

$currentPage = basename($_SERVER['PHP_SELF']);

?>

<head>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
</head>

<nav class="navbar navbar-expand-lg navbar-dark">
    <a href="/"><img class="logo" src="img/clairetyrerlogo.png"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1" aria-controls="navbarNav"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbar1">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="<?php if ($currentPage == 'index.php') {echo 'active';} ?> nav-link" href="index">Home</a>
            </li>
            <li class="nav-item">
                <a id="about" class="<?php if ($currentPage == 'about.php') {echo 'active';} ?> nav-link" href="about">About</a>
            </li>
            <li class="nav-item"><a class="<?php if ($currentPage == 'gallery.php') {echo 'active';} ?> nav-link" href="gallery">Gallery</a></li>
            <li class="nav-item dropdown">
                <a class="<?php if ($currentPage == 'products.php') {echo 'active';} ?> nav-link dropdown-toggle" href="products" data-toggle="dropdown">Products</a>
                <ul class="dropdown-menu">
                    <li><a class="dropdown-item" href="products"> All</a></li>
                    <li><a class="dropdown-item" href="products#wedding"> Wedding</a></li>
                    <li><a class="dropdown-item" href="products#prom"> Prom</a></li>
                    <li><a class="dropdown-item" href="products#occasion"> Occasion</a></li>
                    <li><a class="dropdown-item" href="products#bespoke"> Bespoke</a></li>
                    <li><a class="dropdown-item" href="products#accessories"> Accessories</a></li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="btn ml-2 pinkbtn navbtn" href="contact">Contact Me</a>
            </li>
        </ul>
    </div>
</nav>

<script>
    $(document).ready(function () {
    if(window.location.href.indexOf("about") > -1) {
        var x = document.getElementById("about").addClass("active");
    }
    else if(window.location.href.indexOf("err=1") > -1) {
        emailError();
    }
});
</script>