<!doctype html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127031291-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-127031291-1');
    </script>

    <title>Products | Claire Tyrer: Dressmaker in Looe, Cornwall</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/font.css">
</head>

<body>
    <?php include 'navbar.php' ?>
    <div class="wrap">
        <div class="container-fluid dressHeader">
            <div class="mx-auto">
                <h1 class="text-center titleText">Products and Services</h1>
            </div>
        </div>
        <div class="container">
            <div id="wedding" class="row border-bottom">
                <div class="col-md-6">
                    <img class="dressImg" src="../img/13.jpg">
                </div>
                <div class="col-md-6">
                    <h2 style="color: black" class="text-center">Wedding</h2>
                    <p class="dressText">If you've decided on the dress that you want, I can measure and fit it correctly for you. If you need some help picking the correct size to begin with, I'd be happy to measure you prior to your purchase.<br><br>
                    <small><i>Please note I cannot provide the dresses themselves. If you're unsure of where to start, get in touch and I can help.</i></small></p>
                    <h4 class="text-center">Services Available</h4>
                    <ul>
                    <li>Fitting</li>
                    <li>Dressing Service</li>
                    <li>Repair</li>
                    </ul>
                </div>
            </div>
            <div id="prom" class="row border-bottom">
                <div class="col-md-6 d-block d-md-none">
                    <img class="dressImg" src="../img/8.JPG">
                </div>
                <div class="col-md-6">
                    <h2 style="color: black" class="text-center">Prom</h2>
                    <p class="dressText">Finish education in style; I'll ensure your dress is fitted correctly so you can look your best as you celebrate your movement to the next stage of your life.<br><br>
                    <small><i>Please note I cannot provide the dresses themselves. If you're unsure of where to start, get in touch and I can help.</i></small></p>
                    <h4 class="text-center">Services Available</h4>
                    <ul>
                    <li>Fitting</li>
                    <li>Dressing Service</li>
                    <li>Repair</li>
                    </ul>
                </div>
                <div class="col-md-6 d-none d-md-block">
                    <img class="dressImg" src="../img/8.JPG">
                </div>
            </div>
            <div id="occasion" class="row border-bottom">
                <div class="col-md-6">
                    <img class="dressImg" src="../img/1.jpg">
                </div>
                <div class="col-md-6">
                    <h2 style="color: black" class="text-center">Occasion</h2>
                    <p class="dressText">Fancy a night out to celebrate a promotion? Attending a ball?Maybe you're attending your friend's wedding? I'm also happy to fit and repair dresses for semi-formal and informal occasions.</p>
                    <h4 class="text-center">Services Available</h4>
                    <ul>
                    <li>Fitting</li>
                    <li>Dressing Service</li>
                    <li>Repair</li>
                    </ul>
                </div>
            </div>
            <div id="bespoke" class="row border-bottom">
                <div class="col-md-6 d-block d-md-none">
                    <img class="dressImg" src="../img/19.jpg">
                </div>
                <div class="col-md-6">
                    <h2 style="color: black" class="text-center">Bespoke</h2>
                    <p class="dressText">I have additional experience with creating bespoke outfits for fancy dress and cosplay.</p>
                    <h4 class="text-center">Services Available</h4>
                    <ul>
                    <li>Fitting</li>
                    <li>Repair</li>
                    <li>Dressing service only available for some outfits</li>
                    </ul>
                </div>
                <div class="col-md-6 d-none d-md-block">
                    <img class="dressImg" src="../img/19.jpg">
                </div>
            </div>
            <div id="accessories" class="row">
                <div class="col-md-6 d-none d-md-block">
                    <img class="dressImg" src="../img/alter.jpg">
                </div>
                <div class="col-md-6 d-block d-md-none">
                    <img class="dressImg" src="../img/alter.jpg">
                </div>
                <div class="col-md-6">
                    <h2 style="color: black" class="text-center">Accessories</h2>
                    <p class="dressText">Sometimes an outfit needs something a little extra - maybe a bow, clutch purse, cummerbund, or a shawl? Let me know you'd like something extra and I'll see if it can be done for you.</p>
                    <h4 class="text-center">Services Available</h4>
                    <ul>
                    <li>Creation</li>
                    <li>Alteration</li>
                    <li>Repair</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php include 'footer.php' ?>
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../bootstrap/js/bootstrap.js"></script>
</body>

</html>