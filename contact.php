<?php

session_start();

function generateFormToken($form)
{
    // generate a token from an unique value
    $token = md5(uniqid(microtime(), true));  
    // Write the generated token to the session variable to check it against the hidden field when the form is sent
    $_SESSION[$form . '_token'] = $token;
    return $token;
}

// generate a new token for the $_SESSION superglobal and put them in a hidden field
$newToken = generateFormToken('form1');

function verifyFormToken($form)
{
    // check if a session is started and a token is transmitted, if not return an error
    if (!isset($_SESSION[$form . '_token'])) {
        return false;
    }
	// check if the form is sent with token in it
    if (!isset($_POST['token'])) {
        return false;
    }
	// compare the tokens against each other if they are still the same
    if ($_SESSION[$form . '_token'] !== $_POST['token']) {
        return false;
    }
    return true;
}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127031291-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-127031291-1');
    </script>
    <title>Contact Me | Claire Tyrer: Dressmaker in Looe, Cornwall</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/font.css">
  </head>
  <body>
    <?php include 'navbar.php' ?>
    <div class="wrap">
        <!-- Contact Page Header -->
        <div class="container-fluid contactHeader">
            <div class="mx-auto">
                <h1 class="text-center titleText">Contact Me</h1>
            </div>
        </div>
        <!-- Form -->
        <div class="container contactForm">
            <div class="row">
                <form class="spamStop" method="post" action="mail.php" onsubmit="loadingGif()">
                    <input id="this_title" type="text" name="this_title" value="" />
                    <input id="this_name" type="text" name="this_name" value="" />
                    <input id="this_email" type="text" name="this_email" value="" />
                    <input id="this_company" type="text" name="this_company" value="" />
                </form>
            </div>
            <div class="row">
                <input type="hidden" name="token" value="<?php echo $newToken; ?>">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <p>
                        To discuss an upcoming arrangements where you will need my services, please fill in your details below. I'll get in touch with you as soon as I can.
                    </p>
                    <h4 id="submitConfirm" name="submitConfirm" style="display:none">Thanks for your submission! I'll be in touch soon.</h4>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <form name="contactForm" method="post" action="mail.php" onsubmit="loadingGif()">
                    <div class="form-row">
                        <input type="text" id="name" name="name" class="form-control squareForm" placeholder="Name" required>
                    </div>
                    <div class="form-row">
                        <input type="tel" id="number" name="number" class="form-control squareForm" placeholder="Telephone" required>
                    </div>
                    <div class="form-row">
                        <input type="email" id="email" name="email" class="form-control squareForm" placeholder="Email" required>
                        <!-- <p id="emError" class="emailError">Incorrect email format</p> -->
                    </div>
                    <div class="form-row">
                        <textarea class="form-control squareForm" rows="5" id="request" name="request" placeholder="Enter your request here..." required></textarea>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <button type="submit" name="submit" id="submit" class="btn btn-lg btn-block squarebtn pinkbtn">Submit</button>
                            <img class="gif" id="gif" src="../img/loading.gif">
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </form>
            </div>
            <div class="col-md-2"></div>
            </div>
        </div>
    </div>
    <?php include 'footer.php' ?>
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../bootstrap/js/bootstrap.js"></script>
    <script src="../js/script.js"></script>
  </body>
</html>

<script>
    $(document).ready(function () {
    if(window.location.href.indexOf("submit") > -1) {
        successMessage();
    }
    else if(window.location.href.indexOf("err=1") > -1) {
        emailError();
    }
});
</script>