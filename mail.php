<?php

error_reporting(E_ALL); 

//Checks if POST is being used, if so, collect the data through the strip_input function
if($_SERVER["REQUEST_METHOD"] == "POST") {
    $name       = strip_input($_POST["name"]);
    $number     = strip_input($_POST["number"]);
    $email      = strip_input($_POST["email"]);
    $request    = strip_input($_POST["request"]);
}

//Strips the input data of any potentially harmful content
function strip_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    header('Location: contact.php?err=1');
}

$to         = "sam.aubsmith@gmail.com";
$headers    = "From: $email\r\n";
$headers    .= "Reply-To: $email\r\n";
$headers    .= "MIME-Version: 1.0\r\n";
$headers    .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

$emailFrom      = "admin@clairetyrer.com";
$emailSubject   = "ClaireTyrer.com - Enquiry received through website";

$emailBody = '<style>@font-face{font-family:"Raleway";src:url("https://fonts.googleapis.com/css?family=Raleway")}</style>';
$emailBody .= '<html><body>';
$emailBody .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
$emailBody .= '<tr align="center">';
$emailBody .= '<img src="http://www.clairetyrer.com/img/clairetyrerlogo.png" width="120px">';
$emailBody .= "</tr>";
$emailBody .= '<tr>';
$emailBody .= '<td align="center">';
$emailBody .= '<h1>ClaireTyrer.com - New Enquiry</h1>';
$emailBody .= "<h2>Hi Claire, $name has submitted these details through the website</h2>";
$emailBody .= '</td>';
$emailBody .= '</tr>';
$emailBody .= '<tr>';
$emailBody .= '<td align="center">';
$emailBody .= "<p style='font-family:Raleway;font-size:12pt;'>Name:     $name</p>";
$emailBody .= "<p style='font-family:Raleway;font-size:12pt;'>Number:   $number</p>";
$emailBody .= "<p style='font-family:Raleway;font-size:12pt;'>Email:    $email</p>";
$emailBody .= "<p style='font-family:Raleway;font-size:12pt;'>Request:  $request</p>";
$emailBody .= '</td>';
$emailBody .= '</tr>';
$emailBody .= '</table>';
$emailBody .= '</html></body>';

mail($to, $emailSubject, $emailBody, $headers);

header('Location: contact.php?submit');

?>