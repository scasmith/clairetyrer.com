<!doctype html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127031291-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-127031291-1');
    </script>

    <title>Claire Tyrer: Dressmaker in Looe, Cornwall</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/font.css">
    <link rel="stylesheet" type="text/css" href="../css/fancyboxcss/jquery.fancybox.min.css">
</head>

<body>
    <?php include 'navbar.php' ?>
    <div class="wrap">
        <div class="container-fluid indexBackground">
            <div class="mx-auto">
                <h1 class="text-center titleText">Claire Tyrer</h1>
                <h2>Dressmaker</h2>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row shortDesc">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h1 class="text-center">
                        Bespoke Creations and Alterations by Claire Tyrer
                    </h1><br>
                    <p>
                        With over 20 years of professional dressmaking, seamstress Claire Tyrer offers a wealth of knowledge and experience that can accomodate a wide variety of dressmaking needs, including wedding and occasion attire.
                    </p>
                    <p>
                        Her skills do not end with dresses; Claire is also able to create and alter  accessories such as cummerbunds, ties, drawstring purses, and many more.
                    </p>
                    <p>
                        Claire can offer her services across Devon and Cornwall.
                    </p>
                </div>
                <div class="col-md-2"></div>
            </div>
        <div class="row frontPageImgRow" style="display: none">
            <div class="container">
                <h2 class="text-center" style="color: #323232">Customer Testimonials</h2>
            </div>
            <div class="col-lg-4">
                <a href="../img/8.JPG" data-fancybox="gallery" data-caption="Bespoke Prom Dress">
                    <img src="../img/8.JPG" alt="" title="Click for more info!"/>
                </a>
                <p class="text-center">"...Absolutely brilliant!"</p>
            </div>
            <div class="col-lg-4">
                <a href="../img/3.jpg" data-fancybox="gallery" data-caption="Fitted Bridesmaid dress">
                    <img src="../img/3.jpg" alt="" title="Click for more info!"/>
                </a>
                <p class="text-center">So glad I went with Claire!</p>
            </div>
            <div class="col-lg-4">
                <a href="../img/17.jpg" data-fancybox="gallery" data-caption="Altered Wedding Dress">
                    <img src="../img/17.jpg" alt="" title="Click for more info!"/>
                </a>
                <p class="text-center">A brilliant experience, highly recommended.</p>
            </div>
        </div>
        </div>
        <div class="container-fluid services">
            <div class="row sidePadding">
                <div class="col-lg-4 serviceCol">
                    <h2 class="text-center">Alterations</h2>
                    <h4 class="text-center">Making sure it's just right</h4><br>
                    <p>If 'it almost fits' isn't what you want, I can fine tune the fit of your attire so that it's perfect for you. </p>
                </div>
                <div class="col-lg-4 serviceCol">
                    <h2 class="text-center">Dressing Service</h2>
                    <h4 class="text-center">There for you on the day</h4><br>
                    <p>For occasions such as weddings, I can offer a dressing service to ensure the bride and bridesmaids are dressed and fitted properly for the big day.</p>
                </div>
                <div class="col-lg-4 serviceCol">
                    <h2 class="text-center">Repairs</h2>
                    <h4 class="text-center">Bring your outfit back to life</h4><br>
                    <p>If time has taken it's toll on your clothing, but you just can't bear to throw it away, I may be able to salvage it, or restore it to its former glory.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4"><br>
                    <a class="btn btn-block btn-lg pinkbtn navbtn" href="contact">Contact Me</a>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
        <div class="container">
            <div class="row bespoke">
                <div class="col-lg-6 bespokeText">
                    <h2 style="color: black" class="text-center">Bespoke Creations</h2><br>
                    <p>
                        Sometimes you know exactly what you want, but you just can't find it anywhere!
                    </p>
                    <p>
                        I'm experienced in making bespoke outfits and can discuss your design to bring your ideas to life. This can be for any occasion and with any kind of outfit and it's sure to be unique. 
                    </p>
                    <p>
                        Whether it's for a wedding, prom, or fancy dress, I'll work with you to make your design a reality.
                    </p>
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-5">
                                <a class="btn btn-block btn-lg squarebtn greybtn" href="gallery">More?</a><br>
                        </div>
                        <div class="col-lg-5">
                            <a class="btn btn-block btn-lg pinkbtn navbtn" href="contact">Contact Me</a>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </div>
                <div class="col-lg-6 bespokeImg">
                    <img src="../img/19.jpg">
                </div>
            </div>
        </div>
        <script src="../js/jquery-3.3.1.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../bootstrap/js/bootstrap.js"></script>
        <script src="../js/fancyboxjs/jquery.fancybox.min.js"></script>
        <script src="../js/script.js"></script>
    </div>
    <?php include 'footer.php' ?>
</body>

</html>