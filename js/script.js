$('[data-fancybox="gallery"]').fancybox({
    buttons: [
        'share',
        'fullScreen',
        'close'
    ],
    thumbs: {
        autoStart: false
    },
    animationDuration: 850,
    transitionDuration: 350,
    loop: true,
    animationEffect: "fade",
    height: 100,
    width: auto,
    loop: true
});

function successMessage() {
    var x = document.getElementById("submitConfirm").style.display="block";
}
function loadingGif() {
    var y = document.getElementById("submit").style.display="none";
    var x = document.getElementById("gif").style.display = "block";
} 
function emailError() {
    var x = document.getElementById("emError").style.display = "block";
}