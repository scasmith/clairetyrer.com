<!doctype html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127031291-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-127031291-1');
    </script>

    <title>Gallery | Claire Tyrer: Dressmaker in Looe, Cornwall</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/font.css">
    <link rel="stylesheet" type="text/css" href="../css/fancyboxcss/jquery.fancybox.min.css">
</head>

<body>
    <?php include 'navbar.php' ?>
    <div class="wrap">
        <div class="container-fluid contactHeader">
            <div class="mx-auto">
                <h1 class="text-center titleText">Gallery</h1>
                <h2>See my previous work</h2>
            </div>
        </div>
        <div class="container"><br>
            <div class="row">
                <div class="galleryCol">
                    <a href="../img/1.jpg" data-fancybox="gallery">
                        <img src="../img/thumbs/1.jpg" alt="" />
                    </a>
                    <a href="../img/2.JPG" data-fancybox="gallery">
                        <img src="../img/thumbs/2.jpg" alt="" />
                    </a>
                    <a href="../img/32.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/32.jpg" alt="" />
                    </a>
                    <a href="../img/3.jpg" data-fancybox="gallery">
                        <img src="../img/thumbs/3.jpg" alt="" />
                    </a>
                    <a href="../img/4.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/4.jpg" alt="" />
                    </a>
                    <a href="../img/18.png" data-fancybox="gallery"  >
                        <img src="../img/thumbs/18.jpg" alt="" />
                    </a>
                    <a href="../img/19.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/19.jpg" alt="" />
                    </a>
                </div>
                <div class="galleryCol">
                    <a href="../img/5.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/5.jpg" alt="" />
                    </a>
                    <a href="../img/6.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/6.jpg" alt="" />
                    </a>
                    <a href="../img/7.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/7.jpg" alt="" />
                    </a>
                    <a href="../img/8.JPG" data-fancybox="gallery"  >
                        <img src="../img/thumbs/8.jpg" alt="" />
                    </a>
                    <a href="../img/21.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/21.jpg" alt="" />
                    </a>
                    <a href="../img/22.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/22.jpg" alt="" />
                    </a>
                    <a href="../img/20.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/20.jpg" alt="" />
                    </a>
                    <a href="../img/23.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/23.jpg" alt="" />
                    </a>
                </div>
                <div class="galleryCol">
                    <a href="../img/9.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/9.jpg" alt="" />
                    </a>
                    <a href="../img/10.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/10.jpg" alt="" />
                    </a>
                    <a href="../img/34.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/34.jpg" alt="" />
                    </a>
                    <a href="../img/12.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/12.jpg" alt="" />
                    </a>
                    <a href="../img/24.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/24.jpg" alt="" />
                    </a>
                    <a href="../img/25.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/25.jpg" alt="" />
                    </a>
                    <a href="../img/26.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/26.jpg" alt="" />
                    </a>
                    <a href="../img/31.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/31.jpg" alt="" />
                    </a>
                </div>
                <div class="galleryCol">
                    <a href="../img/13.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/13.jpg" alt="" />
                    </a>
                    <a href="../img/14.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/14.jpg" alt="" />
                    </a>
                    <a href="../img/15.JPG" data-fancybox="gallery"  >
                        <img src="../img/thumbs/15.jpg" alt="" />
                    </a>
                    <a href="../img/27.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/27.jpg" alt="" />
                    </a>
                    <a href="../img/28.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/28.jpg" alt="" />
                    </a>
                    <a href="../img/30.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/30.jpg" alt="" />
                    </a>
                    <a href="../img/35.jpg" data-fancybox="gallery"  >
                        <img src="../img/thumbs/35.jpg" alt="" />
                    </a>
                </div>
            </div><br>
        </div>
    </div>
    <?php include 'footer.php' ?>
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/jquery.js"></script>
    <script src="../js/script.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../bootstrap/js/bootstrap.js"></script>
    <script src="../js/fancyboxjs/jquery.fancybox.min.js"></script>
</body>

</html>