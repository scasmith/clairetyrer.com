<!doctype html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127031291-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-127031291-1');
    </script>

    <title>About | Claire Tyrer: Dressmaker in Looe, Cornwall</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/font.css">
</head>

<body>
    <?php include 'navbar.php' ?>
    <div class="wrap">
        <div class="container-fluid aboutHeader">
            <div class="mx-auto">
                <h1 class="text-center titleText">About</h1>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row aboutRow">
                <div class="col-lg-1"></div>
                <div class="col-lg-3">
                    <img class="centered-and-cropped aboutImg" width="339" height="384" src="../img/claire.png">
                </div>
                <div class="col-lg-7">
                    <h2 style="color: black" class="text-center">Claire Tyrer</h2>
                    <p>
                        My name is Claire Tyrer, and I am from St. Helens, Merseyside. I've recently moved down to Looe, Cornwall with my husband Chris in order to live the next stage of our life in our favourite seaside town.
                        <br><br>
                        I have a wealth of experience from my decades of work as a seamstress for friends, colleagues, and wedding shops. I take pride in my ability to meet the needs of my customers, and thoroughly enjoy seeing them get the outfit they want. Most of my experience is with wedding and prom dresses, but I have plenty of experience in many other areas of fitting and bespoke creation.
                        <br><br>
                        I welcome challenging outfits, but I also know my limits and will always keep you informed in the first instance if something is not possible. 
                    </p>
                </div>
                <div class="col-lg-1"></div>
            </div>
        </div>
    </div>
    <?php include 'footer.php' ?>
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../bootstrap/js/bootstrap.js"></script>
</body>

</html>