<footer class="footer">
    <div class="container-fluid text-center">
        <span>ClaireTyrer.com | Created by <a href="https://www.scasmith.com" target="_blank">S.C.A-Smith</a></span> |
        <small>v1.0</small>
    </div>
</footer>